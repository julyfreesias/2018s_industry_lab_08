package ictgradschool.industry.io.IOtutorial;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class IOtutoral {

    public static void main(String[] args) {

//
//        /*1. scan the input from Keyboard*/
//        Scanner sc = new Scanner(System.in); /* input from console */
//        System.out.print("input integer");
//        int i =sc.nextInt(); /* read input and store value in to i variable*/
//        System.out.println("Your input integer is"+ i );
//        sc.close(); /*close the scanner*/

        /*2. scan the file data*/
        File file = new File("input.txt");
        try {
            Scanner sc = new Scanner(file);
            while(sc.hasNextInt()) {
                System.out.println(sc.nextLine());
            }sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

}
