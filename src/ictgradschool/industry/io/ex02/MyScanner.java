package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import javax.security.auth.kerberos.KerberosKey;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
        System.out.println("Enter a file name :");
        String fileName = Keyboard.readInput();
        File file = new File(fileName);
        try(Scanner sc = new Scanner(file)){
            while(sc.hasNextLine()){
                System.out.println(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
