package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.*;

public class MyReader {

//    public void start() {
//
//        // TODO Prompt the user for a file name, then read and print out all the text in that file.
//        // TODO Use a BufferedReader.
//        System.out.println("Enter a file name :");
//        String text = Keyboard.readInput();
//        File f1 = new File(text);
//        try(BufferedReader reader = new BufferedReader(new FileReader(f1))){
//            String line = null;
//            while((line=reader.readLine()) != null){
//                System.out.println(line);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void start(){

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
//        // TODO Use a BufferedReader.
        System.out.println("Enter a file name :");
        String fileName = Keyboard.readInput();

        File file = new File(fileName);
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;
            while((line=reader.readLine())!=null){
                System.out.println(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        new MyReader().start();
    }
}

