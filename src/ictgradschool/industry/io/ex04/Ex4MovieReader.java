package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        try(Scanner sc =new Scanner(new File(fileName))){

            Movie[] movies = new Movie[19];
            int i =0;
            while(sc.hasNextLine()){

                String name = sc.next();
                int year =sc.nextInt();
                int minutes =sc.nextInt();
                String director = sc.next();
                movies[i] = new Movie(name,year,minutes,director);
                i++;
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }


        return null;
    }



    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}

