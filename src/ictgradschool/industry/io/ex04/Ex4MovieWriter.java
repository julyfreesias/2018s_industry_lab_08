package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        File file = new File("fileName");//read about File

        try(PrintWriter writer = new PrintWriter(new FileWriter(file))) {

            int i = 0;
            while (i < films.length) {


                writer.print(films[i].getName());
                writer.print(films[i].getYear());
                writer.print(films[i].getLengthInMinutes());
                writer.print(films[i].getDirector());
                fileName.split(",");
                i++;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("Movies saved successfully to " + fileName + "!");

    }






    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
