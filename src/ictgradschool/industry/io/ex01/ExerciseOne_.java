package ictgradschool.industry.io.ex01;

import java.io.*;

public class ExerciseOne_ {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }


    //Using FileReader
    //1.total number of Characters in the file
    //2.The number of e's and E's in the file
    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;
        int num = 0;
        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        try (FileReader fR = new FileReader("input2.txt");) {

            while ((num = fR.read()) != -1) {  ///file reader read a byte at once. and end of the line always has -1
                char charN = (char) num;
                if (charN == 'e' || charN == 'E') {
                    numE++;
                }
                total++;
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file not found" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO problem" + e.getMessage());
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }







    //Using BufferedReader
    //1.total number of Characters in the file
    //2.The number of e's and E's in the file
    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;
        int num = 0;
        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        File file = new File("input2.txt");
        try (BufferedReader buffR = new BufferedReader(new FileReader(file));) {
            while ((num = buffR.read()) != -1) {
                char charE = (char) num;
                if (charE == 'e' || charE == 'E') {
                    numE++;
                }
            }total++;
        } catch (IOException e) {
            System.out.println("input error" + e.getMessage());
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {

        new ExerciseOne_().start();
    }

}
