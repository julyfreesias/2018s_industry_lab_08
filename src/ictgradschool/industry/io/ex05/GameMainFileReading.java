package ictgradschool.industry.io.ex05;

import ictgradschool.Keyboard;

import java.io.*;

public class GameMainFileReading {
    public void start() {

        User user = new User();
        Computer bot = new Computer();
        System.out.println("Do you want to 1. manually enter the guesses, or 2. to automatically guess based on pre-supplied guesses in a file?");
        int guessMethod = Integer.parseInt(Keyboard.readInput());
        if (guessMethod == 2) {
            System.out.println("Enter the file name");
            String fname = Keyboard.readInput();
            File f1 = new File(fname);
            int attemptUser = 7;  //total 7 attemps
            int attemptComputer = 7;

            String[] secretCodes = askSecretCode(user, bot);

            while (attemptComputer != 0) {
                // user guess and evaluate bulls and cows.

                boolean userWin = userGuess(bot, user, guessMethod);
                attemptUser = user.attempReduce(attemptUser);
                if (userWin) {
                    System.out.println("You win!");
                    break;
                }
                // computer guess and evaluate bulls and cows
                boolean compWin = computerGuess(bot, user);
                attemptComputer = bot.attempReduce(attemptComputer);
                if (compWin) {
                    System.out.println("Computer wins! Game Over!");
                    break;
                }
            }
        }
    }

    public String[] askSecretCode(User user, Computer bot) {
        String[] codes = new String[2];
        codes[0] = user.getInput(); //print out enter your secret code in the class.
        codes[1] = bot.comRandomNum();
        return codes;
    }


    //print result and assess if the game is over.

    public boolean userGuess(Computer bot, User user, int guessMethod) {
        String userGuess = "";
        if (guessMethod == 1) {
            userGuess = user.getInput();  //get guess from user

        } else {

            while(true){
            System.out.println("Please enter the name of the file with the codes!");
            String fileName = Keyboard.readInput();
            File codeFile = new File(fileName);

            try (BufferedReader bR = new BufferedReader(new FileReader(codeFile))) {
                userGuess = bR.readLine();
                String[] guess = new String[7];
                int i = 0;
                while (userGuess != null) {
                    guess[i] = userGuess;
                    i++;
                }break;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                continue;
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }
    }
            System.out.println("Your guess: "+userGuess);
            System.out.println("=========================");

    int[] botevl = bot.gameEvaluation(userGuess);  //return string or a num array?
    //plural check
    String b = "";
    String c = "";
            if(botevl[0]>1)

    {
        b = "bulls";
    } else

    {
        b = "bull";
    }
            if(botevl[1]>1)

    {
        c = "cows";
    } else

    {
        c = "cow";
    }

            System.out.println("Result: "+botevl[0]+" "+b +" and "+botevl[1]+" "+c);
            if(botevl[0]==4&&botevl[1]==0)

    {

        return true;
    } else

    {
        return false;
    }

}

    public boolean computerGuess(Computer bot, User user) {
        String botGuess = bot.comRandomNum();  //get guess from user
        System.out.println("Computer's guess: " + botGuess);
        System.out.println("---");
//        attemptComputer = bot.attempReduce(attemptComputer);
        int[] userevl = user.gameEvaluation(botGuess);  //return string or a num array?
        //plural check
        String b = "";
        String c = "";
        if (userevl[0] > 1) {
            b = "bulls";
        } else {
            b = "bull";
        }
        if (userevl[1] > 1) {
            c = "cows";
        } else {
            c = "cow";
        }

        System.out.println("Result: " + userevl[0] + " " + b + " and " + userevl[1] + " " + c);
        if (userevl[0] == 4 && userevl[1] == 0) {

            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        GameMainFileReading newGame = new GameMainFileReading();
        newGame.start();
    }
}

