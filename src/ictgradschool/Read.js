function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', './data.json', true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            callback(JSON.parse(xobj.responseText));
        }
    };
    xobj.send(null);
}

loadJSON(function(json) {
    console.log(json); // this will log out the json object
    var s = ""
    for (var i = 0; i < json.length; i++) {
        s+="('" + json[i].name + "', '" + json[i].gender + "', '" + json[i].year_born + "', '" + json[i].joined + "', '" + json[i].num_hires + "'),\n"
    }
    console.log(s);
});



////web11 database read json file
/*
* make read.js file  and read the file using javascript function.
* the data copy and paste in the jason file in the same directory
* and executed and see the console the result*/